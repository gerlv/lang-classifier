# Sentence Classifier

Detect language from a sentence.

## Local development

```bash
docker-compose build # to build the stack
docker-compose up # run the stack (port 8000)
```

Once the docker compose is running navigate to http://localhost:8000/docs

## Deploying application to AWS

Setup below allows you to run the application using AWS Fargate (serverless) infrastructure behind the Application Load Balancer with PostgreSQL (AWS RDS) as database.

### Database

[Setup single instance AWS RDS instance.](https://eu-west-2.console.aws.amazon.com/rds/home?region=eu-west-2#launch-dbinstance:gdb=false;s3-import=false)

- Engine type: PostgreSQL
- Version: 13.4
- Template: production
- DB instance identifier: langclass
- Select - Auto generate password
- DB instance type - burstable classes - db.t3.micro

If you expect large number of requests and application requires higher availability switch to a larger db instance type and set it up as a multi AZ cluster using Aurora PostgreSQL compatible engine.

Copy credentials details, this will be required for populating secrets in system manager.

### ECR

[Create Elastic Container Registry Repository](https://eu-west-2.console.aws.amazon.com/ecr/create-repository?region=eu-west-2)

Once created build and push Docker image as described in the repository `View push commands` popup.

### DATABASE_URL

[Create Parameter Store secret](https://eu-west-2.console.aws.amazon.com/systems-manager/parameters/create?region=eu-west-2&tab=Table) 
for DATABASE_URL.

Select `SecureString` and `Type` as `text`, enter the full url with details from `Database` step:

```
postgresql://<username>:<password>@<endpoint>/<database name>
```

Note down the ARN for this resource.

### IAM role

[Create ECS Exec Role](https://console.aws.amazon.com/iam/home#/roles$new?step=type&selectedService=EC2ContainerService&selectedUseCase=EC2ContainerTaskRole) so the AWS Fargate containers can pull DATABASE secrets and ECR image.

You can use pre-existing AWS ECS Exec permission set and add extra permission for getting SSM parameters.

### AWS Fargate Service

[Create cluster](https://eu-west-2.console.aws.amazon.com/ecs/home?region=eu-west-2#/clusters/create/new)

For more information see this [guide](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create-service-console-v2.html)

Using networking only setup.

After the cluster has been created - add Task Definition:

- Type Fargate
- Select task execution role
- specify task memory and CPU
- Add container definition
  - Specify the image location (from ECR)
  - for port mappings add 8000
  - In the environment variables add `DATABASE_URL` and change `Value` to `ValueFrom` and paste the `DATABASE_URL` secret ARN.

It's further possible to setup autoscaling in case there is a spike in traffic.

### Application Load Balancer

[Create Application Load Balancer](https://eu-west-2.console.aws.amazon.com/ec2/v2/home?region=eu-west-2#SelectCreateELBWizard:)

Follow this [guide](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create-application-load-balancer.html) for more information on how to set it up.

import os
from config import WORDBANK
from trie import Trie


class Classifier:
    def __init__(self, populate=True):
        self.trie = Trie()

    def classify(self, sentence):
        words = sentence.split()
        all_match = all([self.trie.search(word) for word in words])
        return "en-US" if all_match else None

    def populate(self):
        with WORDBANK.open("rt") as wb_file:
            line = wb_file.readline()
            while line:
                line = line.lower().strip()
                self.trie.insert(line)
                line = wb_file.readline()

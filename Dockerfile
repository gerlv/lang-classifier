FROM python:3.9-bullseye
WORKDIR /app
RUN apt-get update && \
    apt-get install -y \
      libpq-dev python3-dev \
      wamerican # en-US words
COPY requirements.txt /app
RUN pip install -r requirements.txt
ENV WORDBANK_FILE_PATH=/usr/share/dict/american-english
COPY config.py main.py trie.py wordbank.py /app
CMD ["uvicorn", "main:app", "--host", "0.0.0.0"]
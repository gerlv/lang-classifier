class TrieNode:
    def __init__(self, letter=""):
        self.letter = letter
        self.kids = {}
        self.is_word = False


class Trie:
    def __init__(self):
        self.root = TrieNode("")

    def insert(self, word):
        if not word:
            return

        node = self.root
        for l in word:
            if l not in node.kids:
                node.kids[l] = TrieNode(l)
            node = node.kids[l]
        node.is_word = True

    def search(self, word):
        if not word:
            return False

        node = self.root
        for l in word.lower():
            if l not in node.kids:
                return False
            node = node.kids[l]

        return node.is_word

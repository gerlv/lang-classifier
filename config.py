import os
from pathlib import Path

DATABASE_URL = os.getenv("DATABASE_URL", "postgresql://postgres@pass@db/requests")

WORDBANK_FILE_PATH = os.getenv("WORDBANK_FILE_PATH", "/usr/share/dict/words")
WORDBANK = Path(WORDBANK_FILE_PATH)

assert WORDBANK.exists()

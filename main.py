import databases
import sqlalchemy
from fastapi import FastAPI, status, HTTPException
from pydantic import BaseModel

from config import DATABASE_URL
from wordbank import Classifier

app = FastAPI()

DATABASE = databases.Database(DATABASE_URL)
engine = sqlalchemy.create_engine(DATABASE_URL)
metadata = sqlalchemy.MetaData()
NOTES = sqlalchemy.Table(
    "requests",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("input", sqlalchemy.String),
    sqlalchemy.Column("result", sqlalchemy.String),
)
metadata.create_all(engine)

CLASSIFIER = Classifier(populate=False)


class Sentence(BaseModel):
    input: str


@app.on_event("startup")
async def setup():
    CLASSIFIER.populate()
    await DATABASE.connect()


@app.on_event("shutdown")
async def shutdown():
    await DATABASE.disconnect()


@app.post("/")
async def check_sentence(sentence: Sentence):
    res = CLASSIFIER.classify(sentence.input)
    query = NOTES.insert().values(input=sentence.input, result=res if res else "")
    await DATABASE.execute(query)

    if not res:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Unsupported word in a sentence",
        )
    return res
